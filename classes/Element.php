<?php

namespace SchuWeb;

class Element extends \ContentElement
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_sample';

    /**
     * Compile the content element
     */
    protected function compile()
    {
        if (TL_MODE == 'BE') {
            $this->genBeOutput();
        } else {
            $this->genFeOutput();
        }
    }

    /**
     * Create backend view
     * @return string
     */
    private function genBeOutput()
    {
        $this->strTemplate          = 'be_wildcard';
        $this->Template             = new \BackendTemplate($this->strTemplate);
        $this->Template->title      = 'Test element';
    }

    /**
     * Create frontend view
     * @return string
     */
    private function genFeOutput()
    {
        $GLOBALS['TL_CSS'][]        = 'system/modules/starting_point/assets/styles.css';
        $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/starting_point/assets/script.js';

        $data = \Database::getInstance()->prepare('SELECT * FROM tl_starting_point')->execute();

        $this->Template->name = $data->name;
    }
}
