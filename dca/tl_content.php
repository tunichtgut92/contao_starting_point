<?php

// The following side provides samples for all available database fields. Though it is in German, you can search for English keywords such as "checkbox", "textarea" etc.: https://easysolutionsit.de/artikel/vorlagen-für-dca-felder.html
// Keep in mind that you have to run localhost/contao/install.php to actually create the database columns that you define in this file.

$GLOBALS['TL_DCA']['tl_content']['palettes']['sample_element'] = '{type_legend},type;test;';

$GLOBALS['TL_DCA']['tl_content']['fields']['test'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['test'],
    'exclude'                 => true,
    'sorting'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50', 'mandatory'=>1),
    'sql'                     => "varchar(255) NOT NULL default ''"
);
