This module is intended to be a starting point for coding a Contao extension.

It includes samples for:

- **Create a content element** (classes/ContentElement.php, templates/ce_sample.html5, dca/tl_content.php, config/autoload.php, config/config.php)
- **Create a new database table** (dca/tl_starting_point.php, config/autoload.php, config/config.php)
- **Use an API to get data from the server through JavaScript** (endpoints/api.php, assets/script.js, templates/ce_sample.html5)
