function callAPI(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function() {
        callback(JSON.parse(this.responseText));
    };
    xhr.send();
}

window.addEventListener('load', function() {
    callAPI('system/modules/starting_point/endpoints/api.php', function(results) {
        if (!results[0]) {
            document.getElementById('marker').innerHTML = '<i>You have to create an entry in the admin area under "Sample Module" in order to see a value.</i>';
            return;
        }
        for (var i=0; i<results.length; i++) {
            document.getElementById('marker').innerHTML = results[i]['name'];
        }
    });
});
