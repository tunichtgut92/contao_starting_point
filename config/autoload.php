<?php

ClassLoader::addNamespaces(array
(
    'SchuWeb',
));

ClassLoader::addClasses(array
(
    'SchuWeb\Element'     => 'system/modules/starting_point/classes/Element.php',
));

TemplateLoader::addFiles(array
(
    'ce_sample'                  => 'system/modules/starting_point/templates',
));
